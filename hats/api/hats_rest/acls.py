import requests
import json

from .keys import PEXELS_API_KEY


def get_photo(color, fabric, style_name):
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    payload = {
        "query": f"{color} {fabric}, {style_name} hat",
        "orientation": "landscape",
        "size": "large",
        "locale": "en-US",
        "per_page": "1",
    }
    r = requests.get(
        url,
        headers=headers,
        params=payload,
    )
    content = json.loads(r.content)

    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": "https://openclipart.org/image/800px/271013"}
