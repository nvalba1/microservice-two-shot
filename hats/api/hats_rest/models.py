from django.db import models
from django.urls import reverse


# Create your models here.
class LocationVO(models.Model):
    """
    The LocationVO model is a value object created from instances of the Location
    model in the wardrobe_api.  Its closet_name property is a string formatted with
    the Location model values 'closet_name - section_number - shelf_number'.  Its
    import_href property is the string value for the url of the location instance.

    """
    closet_name = models.CharField(max_length=1000)
    import_href = models.CharField(max_length=255, unique=True)


class Hat(models.Model):
    """
    The hat model represents a hat object stored in a given location in the wardrobe.
    It has the properties fabric, style_name, color, picture_url, and location.
    The location property is a many-to-one foreignKey value referencing the LocationVO
    model.
    """
    fabric = models.CharField(max_length=255)
    style_name = models.CharField(max_length=255)
    color = models.CharField(max_length=255)
    picture_url = models.URLField(null=True)
    location = models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.style_name

    def get_api_url(self):
        return reverse("api_show_hat", kwargs={"pk": self.pk})
