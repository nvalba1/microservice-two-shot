from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse

from .models import Hat, LocationVO

from common.json import ModelEncoder

from .acls import get_photo

import json

# Create your views here.
class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "closet_name",
        "import_href"
    ]

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "color",
        "fabric",
        "style_name",
    ]

    def get_extra_data(self, o):
        return {"location": o.location.closet_name}

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
    ]
    encoders = {
        "location": LocationVODetailEncoder()
    }

@require_http_methods(["GET"])
def api_list_location_VOs(request):
    """
    Lists location value objects so that closet_name values with closet_name,
    section_number and shelf_number can be called in AddHat.js form.
    """
    if request.method == "GET":
        locations = LocationVO.objects.all()
        return JsonResponse(
            {"location_vos": locations},
            encoder=LocationVODetailEncoder,
        )

@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    """
    Lists all of the hats in the wardrobe.

    Returns a dictionary with a single key "hats" which is a list of hat style
    names and URLS.  Each entry in the list is a dictionary that contains the
    style name of the hat and a link to that hat's information.

    Creates a new hat object at the specified location href.  Photo retrieved by
    get_photo function which makes API call to Pexels.

    {
        "hats": [
            {
                "href": href for hat object instance,
                "color": color,
                "fabric": fabric,
                "style_name": hat,
                "location": closet_name - section_number - shelf_number,
            }
        ]
    }
    """
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        photo = get_photo(content["color"], content["fabric"], content["style_name"])
        content.update(photo)
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_hat(request, pk):
    """
    Returns details for hat object if method is GET.
    Returned value is a dictionary with hat model properties
    including nested dictionary for location.

    Deletes hat object with id matching pk if request method
    is DELETE.

    {
	"href": href,
	"fabric": fabric,
	"style_name": style,
	"color": color,
	"picture_url": url,
	"location": {
		"closet_name": closet_name - section_number - shelf_number,
		"import_href": location url
	}
}

    """
    if request.method == "GET":
        hat = Hat.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
