# Wardrobify

Team:

* Nick Alba - Hats
* Rich Thurman - Shoes

## Design

## Shoes microservice

The Shoes microservice is responsible for managing information about shoes and their respective bins. This microservice is integrated with the Wardrobe microservice to store location information about bins.

Models
BinVO
The BinVO model represents a bin where shoes are stored. It contains the following fields:

external_id: A positive integer representing the unique identifier for the bin in the Wardrobe microservice.
href: A char field storing the URL of the bin in the Wardrobe microservice.
closet_name: A char field representing the name of the closet where the bin is located.
bin_number: A positive small integer representing the bin number.
bin_size: A positive small integer representing the size of the bin.
Shoe
The Shoe model represents a shoe and contains the following fields:

name: A char field representing the name of the shoe.
manufacturer: A char field representing the shoe's manufacturer.
color: A char field representing the shoe's color.
picture_url: A URL field storing the URL of the shoe's picture.
bin: A foreign key field representing the relationship between the Shoe and BinVO models. Each shoe is stored in a bin.
Integration with Wardrobe Microservice
The Shoes microservice works with the Wardrobe microservice to store and manage bin and location information. The Wardrobe microservice has its own models for bins and locations:

Location
The Location model in the Wardrobe microservice represents the location of a closet and contains the following fields:

closet_name: A char field representing the name of the closet.
section_number: A positive small integer representing the section number of the wardrobe.
shelf_number: A positive small integer representing the shelf number.
Bin
The Bin model in the Wardrobe microservice represents a bin where shoes are stored and contains the following fields:

closet_name: A char field representing the name of the closet where the bin is located.
bin_number: A positive small integer representing the bin number.
bin_size: A positive small integer representing the size of the bin.
API Endpoints
The Shoes microservice provides the following API endpoints for managing shoes and their respective bins:

shoes/: Lists all shoes or creates a new shoe (GET, POST).
shoes/<int:shoe_id>/: Retrieves, updates or deletes a shoe by its ID (GET, DELETE).
The Wardrobe microservice provides the following API endpoints for managing locations and bins:

locations/: Lists all locations or creates a new location (GET, POST).
locations/<int:pk>/: Retrieves, updates, or deletes a location by its ID (GET, PUT, DELETE).
bins/: Lists all bins or creates a new bin (GET, POST).
bins/<int:pk>/: Retrieves, updates, or deletes a bin by its ID (GET, PUT, DELETE).

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

The Hats microservice is responsible for managing information about hats and their associated locations.  This microservice polls from the Wardrobe API microservice to retrieve information
for location instances to create location value objects which provide the location object instance URL to each Hat instance in the form of a many-to-one relationship.

Models
LocationVO
The LocationVO model represents a hat location.  The wardrobe resource from which location value objects are pulled stores Location object instances with model properties including string values for
the closet name, section number, and shelf number.  The values polled from the Location model instances include the location instance href and a concatenated string description including the closet name,
section number, and shelf number.

The Hats microservice provides the following API endpoints for managing hats and their respective locations:
hats/: Lists all hats or creates a new hat (GET, POST).
hats/<int:hat_id>/: Retrieves, updates, or deletes a hat by its ID (GET, DELETE, PUT).
hats/locations/: Lists all LocationVO model instances.

Front-End Application
The Single Page Application located in ghi/app/src renders all React components in the App.js file.  Child components are HatPage.js and ShoePage.js.  Each of these files is where the state of their respective clothing items is set using the useState function.  Also in each page are where callback functions are declared for deleting an item and reloading the item list upon successful creation of a new item.  Child components called in each of the main clothing item pages include those which provide a form to add a new item and those which list all instances of the clothing item with buttons allowing deletion of the object instance.
