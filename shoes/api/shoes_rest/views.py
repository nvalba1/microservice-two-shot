from django.shortcuts import render

from django.http import JsonResponse, HttpResponse
from django.views.decorators.http import require_http_methods
import json

from .models import Shoe, BinVO

from common.json import ModelEncoder

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "name",
        "manufacturer",
        "color",
        "picture_url",
        "bin",
        ]

    def default(self, obj):
        if isinstance(obj, BinVO):
            return {
                "id": obj.id,
                "closet_name": obj.closet_name,
                "bin_number": obj.bin_number,
                "bin_size": obj.bin_size,
                "href": obj.href,
            }
        return super().default(obj)



@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    """
    This view serves two purposes:
    1. Returns a list of shoes as JSON when the request method is GET.
    2. Creates a new shoe when the request method is POST.


    The JSON response for a GET request has the following format:
    {
        "shoes": [
            {
                "id": shoe_id,
                "name": shoe_name,
                "manufacturer": shoe_manufacturer,
                "color": shoe_color,
                "picture_url": shoe_picture_url,
                "bin_id": shoe_bin_id
            },
            ...
        ]
    }

    For a POST request, the view expects the following fields in the request body:
    - name
    - manufacturer
    - color
    - picture_url
    - bin_id

    Upon successful creation, the view returns a JSON response with a success message and a 201 Created status.
    """
    if request.method == "GET":
        shoes = Shoe.objects.all()
        shoes_data = json.loads(json.dumps(list(shoes), cls=ShoeListEncoder))
        return JsonResponse({"shoes": shoes_data}, safe=False)

    else:
        content = json.loads(request.body)
        try:
            name = content["name"]
            manufacturer = content["manufacturer"]
            color = content["color"]
            picture_url = content["picture_url"]
            bin_id = content["bin_id"]

            bin_instance = BinVO.objects.get(bin_number=bin_id)

            shoe = Shoe.objects.create(
                name=name,
                manufacturer=manufacturer,
                color=color,
                picture_url=picture_url,
                bin=bin_instance
            )
            shoe.save()

            return JsonResponse({"message": "Shoe created successfully"}, status=201)

        except KeyError as e:
            return HttpResponse(f"Missing required field: {str(e)}", status=400)
        except BinVO.DoesNotExist:
            return HttpResponse(f"Bin with id {bin_id} does not exist", status=404)


@require_http_methods(["GET", "DELETE"])
def api_shoe_detail(request, shoe_id):
    """
    Returns the details of a shoe as JSON or deletes the shoe.
    The shoe is identified by its primary key (shoe_id).

    The JSON response has the following format:
    {
        "id": shoe_id,
        "name": shoe_name,
        "manufacturer": shoe_manufacturer,
        "color": shoe_color,
        "picture_url": shoe_picture_url,
        "bin_id": shoe_bin_id
    }
    """
    try:
        shoe = Shoe.objects.get(id=shoe_id)
    except Shoe.DoesNotExist:
        raise Http404("Shoe not found")

    if request.method == "GET":
        shoe_data = {
            "id": shoe.id,
            "name": shoe.name,
            "manufacturer": shoe.manufacturer,
            "color": shoe.color,
            "picture_url": shoe.picture_url,
            "bin_id": shoe.bin.bin_number,
        }
        return JsonResponse(shoe_data)
    elif request.method == "DELETE":
        shoe.delete()
        return HttpResponse(status=204)
