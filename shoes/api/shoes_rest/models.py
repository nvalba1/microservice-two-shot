from django.db import models
from django.urls import reverse


class BinVO(models.Model):
    external_id = models.PositiveIntegerField()
    href = models.CharField(max_length=100, null=True)
    closet_name = models.CharField(max_length=100)
    bin_number = models.PositiveSmallIntegerField()
    bin_size = models.PositiveSmallIntegerField()

    def __str__(self):
        return f"{self.closet_name} - {self.bin_number}/{self.bin_size}"

    class Meta:
        ordering = ("closet_name", "bin_number", "bin_size")


class Shoe(models.Model):
    name = models.CharField(max_length=255)
    manufacturer = models.CharField(max_length=255)
    color = models.CharField(max_length=255)
    picture_url = models.URLField(null=True)
    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return f"{self.name}"

    def get_api_url(self):
        return reverse("api_shoe_detail", kwargs={"pk": self.pk})
