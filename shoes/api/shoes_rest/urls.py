from django.urls import path
from . import views

urlpatterns = [
    path('shoes/', views.api_list_shoes, name='shoe_list_create'),
    path('shoes/<int:shoe_id>/', views.api_shoe_detail, name="shoe_detail"),
]
