import React, { useState, useEffect } from 'react';
import HatList from './HatList';
import AddHatForm from './AddHatForm';


function HatPage() {

  const [hats, setHats] = useState([]);

  const loadHats = async () => {
    const response = await fetch('http://localhost:8090/api/hats');
    const data = await response.json();
    setHats(data.hats);
  };

  const handleDelete = async (hatHref) => {
    const response = await fetch(`http://localhost:8090/${hatHref}`, {
      method: 'DELETE',
    });

    if (response.status === 200) {
      console.log('Hat deleted successfully');
      // Reload the hats list after successful deletion
      loadHats();
    } else {
      console.log('Error deleting hat');
    }
  };

  useEffect(() => {
    loadHats();
  }, []);

  return (
    <div>
      <h1>Hats</h1>
      <AddHatForm onHatAdded={loadHats} />
      <HatList hats={hats} onDelete={handleDelete} />
    </div>
  );
}

export default HatPage;
