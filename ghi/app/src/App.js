import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoePage from './ShoePage'; // Import the ShoePage component
import HatPage from './HatPage'; // Import the HatPage component

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/shoes" element={<ShoePage />} /> // Add route for the ShoePage
          <Route path="/hats" element={<HatPage />} /> // Add route for the HatPage
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
