import React from 'react';

const HatList = ({ hats, onDelete}) => {
  return (
    <div>
      <h2>Hat List</h2>
      <ul>
        {
          hats.map((hat) => (
            <li key={hat.href}>
              {hat.color} - {hat.fabric} - {hat.style_name} Location: {hat.location}
              <button onClick={() => onDelete(hat.href)}>Delete</button>
            </li>
          ))
        }
      </ul>
    </div>
  );
};

export default HatList;
