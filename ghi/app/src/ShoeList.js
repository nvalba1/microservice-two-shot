import React from 'react';

const ShoeList = ({ shoes, onDelete }) => {
  return (
    <div>
      <h2>Shoe List</h2>
      <ul>
        {
          shoes.map((shoe) => (
            <li key={shoe.id}>
              {shoe.name} - {shoe.manufacturer} - {shoe.color}
              <button onClick={() => onDelete(shoe.id)}>Delete</button>
            </li>
          ))
        }
      </ul>
    </div>
  );
};

export default ShoeList;
