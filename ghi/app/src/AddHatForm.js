import React, { useState, useEffect } from 'react';

const AddHatForm = ({ onHatAdded }) => {
  const [formData, setFormData] = useState({
    fabric: '',
    style_name: '',
    color: '',
    location: '',
  });

  const [locations, setLocations] = useState([]);

  const fetchLocations = async () => {
    try {
      const response = await fetch('http://localhost:8090/api/hats/locations');
      if (!response.ok) {
        throw new Error(`HTTP error: ${response.status}`);
      }
      const data = await response.json();
      console.log(data);
      setLocations(data.location_vos);
    } catch (error) {
      console.error('Error fetching locations:', error);
    }
  };

  useEffect(() => {
    fetchLocations();
  }, []);

  const handleChange = (e) => {
    setFormData({...formData, [e.target.name]: e.target.value});
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    const response = await fetch('http://localhost:8090/api/hats/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(formData),
    });

    if (response.status === 200) {
      console.log('Hat added successfully');
      onHatAdded();
    } else {
      console.log('Error adding hat');
    }
  };

  return (
    <form onSubmit={handleSubmit}>
      <h2>Add Hat</h2>
      <div>
        <label htmlFor="fabric">Fabric:</label>
        <input type="text" id="fabric" name="fabric" value={formData.fabric} onChange={handleChange} />
      </div>
      <div>
        <label htmlFor="style_name">Style:</label>
        <input type="text" id="style_name" name="style_name" value={formData.style_name} onChange={handleChange} />
      </div>
      <div>
        <label htmlFor="color">Color:</label>
        <input type="text" id="color" name="color" value={formData.color} onChange={handleChange} />
      </div>
      <div>
        <label htmlFor="location">Location</label>
        <select id="location" name="location" value={formData.location} onChange={handleChange}>
          <option value="">Select a location</option>
          {locations.map((location) => {
            return <option key={location.import_href} value={location.import_href}>{location.closet_name}</option>;
          })}
        </select>
      </div>
      <button type="submit">Add Hat</button>
    </form>
  )
}
export default AddHatForm;
