import React, { useState, useEffect } from 'react';
import ShoeList from './ShoeList';
import AddShoeForm from './AddShoeForm';

function ShoePage() {

  const [shoes, setShoes] = useState([]);

  const loadShoes = async () => {
    const response = await fetch('http://localhost:8080/api/shoes/');
    const data = await response.json();
    setShoes(data.shoes);
  };

  const handleDelete = async (shoeId) => {
    const response = await fetch(`http://localhost:8080/api/shoes/${shoeId}`, {
      method: 'DELETE',
    });

    if (response.status === 204) {
      console.log('Shoe deleted successfully');
      // Reload the shoes list after successful deletion
      loadShoes();
    } else {
      console.log('Error deleting shoe');
    }
  };

  useEffect(() => {
    loadShoes();
  }, []);

  return (
    <div>
      <h1>Shoes</h1>
      <AddShoeForm onShoeAdded={loadShoes} />
      <ShoeList shoes={shoes} onDelete={handleDelete} />
    </div>
  );
}

export default ShoePage;
