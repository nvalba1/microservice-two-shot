import React, { useState, useEffect } from 'react';

const AddShoeForm = ({ onShoeAdded }) => {
  const [formData, setFormData] = useState({
    name: '',
    manufacturer: '',
    color: '',
    picture_url: '',
    bin_id: '',
  });

  const [bins, setBins] = useState([]);

  const fetchBins = async () => {
    try {
      const response = await fetch('http://localhost:8100/api/bins/');
      if (!response.ok) {
        throw new Error(`HTTP error: ${response.status}`);
      }
      const data = await response.json();
      setBins(data.bins);
    } catch (error) {
      console.error('Error fetching bins:', error);
    }
  };

  useEffect(() => {
    fetchBins();
  }, []);

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };




  const handleSubmit = async (e) => {
    e.preventDefault();
    const response = await fetch('http://localhost:8080/api/shoes/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(formData),
    });

    if (response.status === 201) {
      console.log('Shoe added successfully');
      onShoeAdded();
    } else {
      console.log('Error adding shoe');
    }
  };

  return (
    <form onSubmit={handleSubmit}>
      <h2>Add Shoe</h2>
      <div>
        <label htmlFor="name">Name:</label>
        <input type="text" id="name" name="name" value={formData.name} onChange={handleChange} />
      </div>
      <div>
        <label htmlFor="manufacturer">Manufacturer:</label>
        <input type="text" id="manufacturer" name="manufacturer" value={formData.manufacturer} onChange={handleChange} />
      </div>
      <div>
        <label htmlFor="color">Color:</label>
        <input type="text" id="color" name="color" value={formData.color} onChange={handleChange} />
      </div>
      <div>
        <label htmlFor="picture_url">Picture URL:</label>
        <input type="text" id="picture_url" name="picture_url" value={formData.picture_url} onChange={handleChange} />
      </div>
      <div>
        <label htmlFor="bin_id">Bin ID:</label>
        <select id="bin_id" name="bin_id" value={formData.bin_id} onChange={handleChange}>
          <option value="">Select a bin</option>
          {bins.map((bin) => (
            <option key={bin.bin_number} value={bin.bin_number}>
              {bin.bin_number}
            </option>
          ))}
        </select>
      </div>
      <button type="submit">Add Shoe</button>
    </form>
  );
};
export default AddShoeForm;
